1. Word count

Create a map with word as a key and number of occurrences as value. Traverse the file and increment the value for a corresponding key. O(n) complexity.
Assuming that misspelled words occur rarely we could implement a fuzzy search algorithm and going from the least frequent word try to find a correct key for it.

FileInfo.java

2. Intersection

Create a hash set from one of the lists, then go through the other list checking if a hash of a given element is in the set. If yes - print.

Intersection.java

3. Barber shop

Deadlock - processes A and B both need resources 1 and 2. A owns 1, B owns 2 so neither process can continue. This won’t happen here - barber doesn’t have any dependencies.
Starvation - thread can’t continue because other threads are in possession of a resource. Shouldn’t happen here - the operation is quick.

Barber.java

4. Robot

Robot.java

5. Cache

Cache.java

When a request for A misses the cache we need to put it in the cache. If there is some memory left - no problem. If the cache is full we need to evict an element to make space for the new one. LRU will evict the least recently used item. Eviction strategy - which elements to remove from cache and when. FIFO, time based, LRU, least frequently used.
it will work in o(1) because of hash map
by letting just one client update the cache at a given time. In java we could do that by using concurrent hash map
consistency - we must make sure that the changes in one instance are propagated to all others (all clients see the same data). Strict consistency means a lot of overhead for distributed systems so going with eventual consistency would be better.



