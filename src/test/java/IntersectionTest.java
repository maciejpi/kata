import org.junit.Test;

import java.util.List;

import static java.util.Collections.emptyList;

public class IntersectionTest {

    @Test
    public void shouldNotPrintAnythingWhenOneListIsEmpty() {
        Intersection intersection = new Intersection<>(emptyList(), List.of(1));

        intersection.print();
    }

    @Test
    public void shouldPrintIntersection() {
        Intersection intersection = new Intersection<>(List.of(3), List.of(1,2,3));

        intersection.print();
    }
}