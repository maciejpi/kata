import org.scalatest.{FlatSpec, Matchers}

class RobotTest extends FlatSpec with Matchers {

  "Robot" should "have one way to climb one step" in {
    def robot = new Robot

    robot.distinctWays(1) should be (1)
  }

  it should "have two ways to climb two steps" in {
    def robot = new Robot

    robot.distinctWays(2) should be (2)
  }

  it should "have five ways to climb four steps" in {
    def robot = new Robot

    robot.distinctWays(4) should be (5)
  }
}
