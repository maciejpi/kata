import java.util.LinkedList;

public class Barber {

    private final int chairsInWaitingRoom;
    private final LinkedList<String> queue = new LinkedList<>();

    private boolean sleeping = true;

    Barber(int chairs) {
        chairsInWaitingRoom = chairs;
    }

    synchronized void enqueue(String name) {
        if (sleeping) {
            sleeping = false;
            //first client, start working right away (in a way that is not blocking here)
        } else {
            if (queue.size() == chairsInWaitingRoom) throw new WaitingRoomFull();
            queue.addLast(name);
        }
    }
}
