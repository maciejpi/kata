import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Intersection<T> {

    private final Set<T> uniqueItemsInFirst;
    private final List<T> second;

    public Intersection(List<T> first, List<T> second) {
        this.uniqueItemsInFirst = new HashSet<>(first);
        this.second = second;
    }

    void print() {
        second.stream()
                .filter(uniqueItemsInFirst::contains)
                .forEach(System.out::println);
    }
}
