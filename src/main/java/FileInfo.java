import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class FileInfo {

    private final Path path;

    public FileInfo(String path) {
        this.path = Paths.get(path);
    }

    public Map<String, Long> wordCount() {
        Map<String, Long> result = new HashMap<>();
        try (BufferedReader br = Files.newBufferedReader(path)) {
            result = br
                    .lines()
                    .collect(groupingBy(it -> it, counting()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
