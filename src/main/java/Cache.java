import java.util.HashMap;
import java.util.Map;

public class Cache {

    private final Map<String, Integer> cache = new HashMap<>();
    private final SomethingWithSlowAccess something = new SomethingWithSlowAccess();

    int get(String key) {
        if (!cache.containsKey(key)) {
            cache.put(key, something.computeValue(key));
        }
        return cache.get(key);
    }

    void set(String key, int value) {
        cache.put(key, value);
    }

    void remove(String key) {
        cache.remove(key);
    }
}
