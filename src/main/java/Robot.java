public class Robot {

    int distinctWays(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return distinctWays(n - 1) + distinctWays(n - 2);
        }
    }
}
